import io.scenarium.pluginManager.PluginsSupplier;
import io.scenarium.test.Plugin;

module io.scenarium.audio {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	requires transitive io.scenarium.flow;
	requires io.scenarium.image;
	requires javafx.controls;
	requires org.scenicview.scenicview;
	requires javafx.media;
	requires io.scenarium.record;

	exports io.scenarium.test;
	exports io.scenarium.test.operator.test;
	exports io.scenarium.test.operator.test.io;

}
