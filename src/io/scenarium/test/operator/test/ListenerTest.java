/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.operator.test;

import java.util.Arrays;

import io.scenarium.flow.EvolvedOperator;
import io.scenarium.test.internal.Log;

public class ListenerTest extends EvolvedOperator {

	@Override
	public void birth() throws Exception {
		onStart(() -> Log.info(getBlockName() + ": start"));
		onResume(() -> Log.info(getBlockName() + ": resume"));
		onPause(() -> Log.info(getBlockName() + ": pause"));
		onStop(() -> Log.info(getBlockName() + ": stop"));
		// onRecording();
		addBlockNameChangeListener((oldBeanDesc, newBeanDesc) -> Log.info(getBlockName() + ": name changed from: " + oldBeanDesc.name + " to: " + newBeanDesc.name));
		addDeclaredInputChangeListener((names, types) -> Log.info(getBlockName() + ": declaredInput changed to: " + Arrays.toString(names) + " " + Arrays.toString(types)));
		// addRecordingChangeListener(isRecording -> Log.info(getBlockName() + ": Recording state changed to: " + isRecording));
	}

	public Integer process() {
		return null;
	}

	@Override
	public void death() throws Exception {}

}
