/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.operator.test;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

public class LifeGame {
	private BufferedImage[] lifeRaster;
	private int width = 300;
	private int height = 300;
	private BufferedImage nextRaster;
	private BufferedImage currentRaster;
	private double ratio = 0.5;
	private boolean hasChanged;
	private double patate;

	public void birth() {
		this.hasChanged = false;
		this.lifeRaster = new BufferedImage[2];
		this.lifeRaster[0] = new BufferedImage(this.width, this.height, BufferedImage.TYPE_BYTE_GRAY);
		this.lifeRaster[1] = new BufferedImage(this.width, this.height, BufferedImage.TYPE_BYTE_GRAY);
		BufferedImage raster = this.lifeRaster[0];
		byte[] bytes = ((DataBufferByte) raster.getRaster().getDataBuffer()).getData();
		for (int i = 0; i < bytes.length; i++)
			bytes[i] = (byte) (Math.random() < this.ratio ? 0 : 255);
		this.currentRaster = null;
	}

	public void death() {}

	public int getHeight() {
		return this.height;
	}

	public double getPatate() {
		return this.patate;
	}

	public double getRatio() {
		return this.ratio;
	}

	public int getWidth() {
		return this.width;
	}

	private int isAlive(byte[] bytesCurr, int x, int y) {
		if (x >= 0 && x < this.width && y >= 0 && y < this.height)
			return bytesCurr[x + y * this.width] != 0 ? 1 : 0;
		return 0;
	}

	public BufferedImage process(BufferedImage raster) {
		if (this.hasChanged)
			birth();
		boolean isFirstProcess = this.currentRaster == null;
		this.currentRaster = this.lifeRaster[0] == this.currentRaster ? this.lifeRaster[1] : this.lifeRaster[0];
		this.nextRaster = this.currentRaster == this.lifeRaster[0] ? this.lifeRaster[1] : this.lifeRaster[0];
		byte[] bytesCurr = ((DataBufferByte) this.currentRaster.getRaster().getDataBuffer()).getData();
		byte[] bytesNext = ((DataBufferByte) this.nextRaster.getRaster().getDataBuffer()).getData();
		if (isFirstProcess) {
			for (int i = 0; i < bytesCurr.length; i++)
				bytesNext[i] = bytesCurr[i];
			return this.currentRaster;
		}
		for (int x = 0; x < this.width; x++)
			for (int y = 0; y < this.height; y++) {
				int nbrNeighbours = isAlive(bytesCurr, x - 1, y - 1) + isAlive(bytesCurr, x - 1, y + 1) + isAlive(bytesCurr, x + 1, y - 1) + isAlive(bytesCurr, x + 1, y + 1)
						+ isAlive(bytesCurr, x, y + 1) + isAlive(bytesCurr, x + 1, y) + isAlive(bytesCurr, x - 1, y) + isAlive(bytesCurr, x, y - 1);
				if (nbrNeighbours == 2)
					bytesNext[x + y * this.width] = bytesCurr[x + y * this.width];// nextRaster.set(x, y, 0, currentRaster.get(x, y, 0));
				else if (nbrNeighbours == 3)
					bytesNext[x + y * this.width] = (byte) 255;// nextRaster.set(x, y, 0, (byte) 255);
				else if (nbrNeighbours < 2)
					bytesNext[x + y * this.width] = 0;// nextRaster.set(x, y, 0, (byte) 0);
				else if (nbrNeighbours > 3)
					bytesNext[x + y * this.width] = 0;// nextRaster.set(x, y, 0, (byte) 0);
			}
		return this.nextRaster;
	}

	public void setHeight(int height) {
		this.height = height;
		this.hasChanged = true;
	}

	public void setPatate(double patate) {
		this.patate = patate;
	}

	public void setRatio(double ratio) {
		this.ratio = ratio;
		this.hasChanged = true;
	}

	public void setWidth(int width) {
		this.width = width;
		this.hasChanged = true;
	}
}
