/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.operator.test.io;

import java.beans.PropertyChangeListener;
import java.util.Arrays;

import io.beanmanager.editors.primitive.number.NumberInfo;
import io.scenarium.flow.EvolvedOperator;
import io.scenarium.flow.ParamInfo;
import io.scenarium.test.internal.Log;

public class EvolvedIOTest extends EvolvedOperator {
	private SubMyOp prop;
	private int ours = 5;
	private int p1 = 5;
	private int p2 = 5;
	@NumberInfo(min = 0)
	private int nbDynamicInput = 2;
	@NumberInfo(min = 0)
	private int nbDynamicOutput = 3;

	@Override
	public void birth() {
		new Thread(() -> {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			process(0.0, (short) 0);
		}).start();

		// Timer timer = new Timer(getBlockName() + "-Timer");
		// onStart(() -> timer.scheduleAtFixedRate(new TimerTask() {
		// @Override
		// public void run() {
		// Object[] vec = generateOuputsVector();
		// vec[getOutputIndex("Boolean")] = (boolean) (EvolvedIOTest.this.nbDynamicInput == 1 ? true : false);
		// vec[getOutputIndex("2")] = 7;
		// for (int i = 2; i < vec.length; i++) {
		// vec[i] = Float.valueOf(7);
		// }
		// triggerOutput(vec, System.currentTimeMillis());
		// }
		// }, 0, 10));

	}

	@Override
	public void death() {}

	@Override
	public void updateIOStructure() {
		String[] names = new String[this.nbDynamicInput];
		Class<?>[] types = new Class<?>[this.nbDynamicInput];
		for (int i = 0; i < names.length; i++) {
			if (names.length == 1)
				names[i] = "vi-0541254";
			else
				names[i] = "di-" + i;
			types[i] = Boolean.class;
		}
		updateInputs(names, types);

		names = new String[this.nbDynamicOutput];
		types = new Class<?>[this.nbDynamicOutput];
		for (int i = 0; i < names.length; i++) {
			if (names.length == 1)
				names[i] = "do";
			else
				names[i] = "do-" + i;
			types[i] = Float.class;
		}
		updateOutputs(names, types);
	}

	@ParamInfo(in = "vi-0541254y", out = "so")
	public Boolean process(Double si, Short... vi) {
		// Log.info("get data");
		// Object[] addInputs = getAdditionalInputs();
		// Log.info(Arrays.toString(addInputs));
		// if (addInputs != null && addInputs[0] != null)
		// Log.info("ts: " + getTimeStamp(3));
		Object[] vec = generateOuputsVector();
		vec[getOutputIndex("so")] = (boolean) (this.nbDynamicInput == 1 ? true : false);
		for (int i = 2; i < vec.length; i++)
			vec[i] = Float.valueOf(i * 3);
		triggerOutput(vec, System.currentTimeMillis());
		return null;
	}

	// public int getOutput2() {
	// return this.prop.getVache() + 7;
	// }

	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		if (Arrays.asList(this.pcs.getPropertyChangeListeners()).contains(listener))
			Log.info("remove ok");
		this.pcs.removePropertyChangeListener(listener);
	}

	public int getNbDynamicInput() {
		return this.nbDynamicInput;
	}

	public void setNbDynamicInput(int nbDynamicInput) {
		int oldValue = this.nbDynamicInput;
		this.nbDynamicInput = nbDynamicInput;
		this.pcs.firePropertyChange("nbDynamicInput", oldValue, nbDynamicInput);
		updateIOStructure();
	}

	public int getNbDynamicOutput() {
		return this.nbDynamicOutput;
	}

	public void setNbDynamicOutput(int nbDynamicOutput) {
		int oldValue = this.nbDynamicOutput;
		this.nbDynamicOutput = nbDynamicOutput;
		this.pcs.firePropertyChange("nbDynamicOutput", oldValue, nbDynamicOutput);
		updateIOStructure();
	}

	public SubMyOp getProp() {
		return this.prop;
	}

	public void setProp(SubMyOp prop) {
		SubMyOp oldProp = this.prop;
		this.prop = prop;
		this.pcs.firePropertyChange("prop", oldProp, prop);
	}

	public int getOurs() {
		return this.ours;
	}

	public void setOurs(int ours) {
		int oldValue = this.ours;
		this.ours = ours;
		this.pcs.firePropertyChange("ours", oldValue, ours);
	}

	public int getP1() {
		return this.p1;
	}

	public void setP1(int p1) {
		var oldValue = this.p1;
		this.p1 = p1;
		this.pcs.firePropertyChange("p1", oldValue, p1);
	}

	public int getP2() {
		return this.p2;
	}

	public void setP2(int p2) {
		var oldValue = this.p2;
		this.p2 = p2;
		this.pcs.firePropertyChange("p2", oldValue, p2);
	}
}
