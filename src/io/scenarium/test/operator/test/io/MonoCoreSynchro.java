package io.scenarium.test.operator.test.io;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import io.scenarium.flow.EvolvedOperator;
import io.scenarium.test.internal.Log;

public class MonoCoreSynchro extends EvolvedOperator {
	private Thread t;
	private final ReentrantLock l = new ReentrantLock();
	private final Condition dataAvailable = this.l.newCondition();
	private final Condition dataConsumed = this.l.newCondition();
	private Byte data;

	@Override
	public void birth() throws Exception {
		this.t = new Thread(() -> {
			while (true) {
				Byte cdata;
				this.l.lock();
				try {
					if (this.data == null)
						try {
							Log.info("Son await");
							this.dataAvailable.await();
							Log.info("Son await done");
						} catch (InterruptedException e) {
							Log.error("Son interrupted");
							return;
						}
					cdata = (byte) (this.data + 7);
					Log.info("Son signal");
					this.data = null;
					this.dataConsumed.signal();
				} finally {
					this.l.unlock();
				}
				// runLater(() -> {
				triggerOutput(cdata);
				// });
			}
		});
		this.t.setName("Son");
		this.t.start();
	}

	public Byte process(Byte a) {
		this.l.lock();
		try {
			this.data = a;
			Log.info("Father signal");
			this.dataAvailable.signal();
			try {
				Log.info("Father await");
				this.dataConsumed.await();
				Log.info("Father await done");
			} catch (InterruptedException e) {
				Log.error("Father interrupted");
			}
		} finally {
			this.l.unlock();
		}
		return null;
	}

	@Override
	public void death() throws Exception {
		this.t.interrupt();
		this.t = null;
	}
}