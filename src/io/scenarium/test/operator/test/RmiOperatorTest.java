/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.operator.test;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import io.scenarium.flow.EvolvedOperator;
import io.scenarium.test.internal.Log;

public class RmiOperatorTest extends EvolvedOperator {
	private int patate;
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		// Log.info("addPropertyChangeListener from " + ProcessHandle.current().pid());
		this.pcs.addPropertyChangeListener(listener);
	}

	@Override
	public void birth() {
		Log.info("birth of " + getBlockName() + ", pid: " + ProcessHandle.current().pid() + " patate: " + this.patate + " isAlive: " + isAlive() + " running: " + isRunning());
		addBlockNameChangeListener((old, newOne) -> Log.info(newOne.name + " From: " + ProcessHandle.current().pid()));
		addInputLinksChangeListener((indexOfInput) -> Log.info("InputLinksChangeListener: " + indexOfInput + " From: " + ProcessHandle.current().pid()));
		// onStart(() -> Log.info("Started from " + ProcessHandle.current().pid() + " isAlive: " + isAlive() + " running: " + isRunning()));
		// onResume(() -> Log.info("Resumed from " + ProcessHandle.current().pid() + " isAlive: " + isAlive() + " running: " + isRunning()));
		// onPause(() -> Log.info("Paused from " + ProcessHandle.current().pid() + " isAlive: " + isAlive() + " running: " + isRunning()));
		// onStop(() -> Log.info("Stopped from " + ProcessHandle.current().pid() + " isAlive: " + isAlive() + " running: " + isRunning()));
		setPatate(10);
	}

	@Override
	public void death() {
		Log.info("death of" + getBlockName() + ", pid: " + ProcessHandle.current().pid() + " patate: " + this.patate + " isAlive: " + isAlive() + " running: " + isRunning());
	}

	public int getPatate() {
		return this.patate;
	}

	public Integer process(Byte i, Integer j, Byte... k) {
		Log.info("Process from " + ProcessHandle.current().pid() + " isAlive: " + isAlive() + " running: " + isRunning());
		// Log.info("TimeStamp: " + getTimeStamp(0));
		// Log.info("process, pid: " + ProcessHandle.current().pid() + " patate: " + patate);
		if (i == null || j == null || k == null)
			return 5;
		int res = i + j;
		for (int l = 0; l < k.length; l++)
			res += k[l] == null ? 0 : k[l];
		return res;
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	public void setPatate(int patate) {
		// Log.info("setPatate, pid: " + ProcessHandle.current().pid() + " patate: " + patate);
		int oldPatate = this.patate;
		this.patate = patate;
		this.pcs.firePropertyChange("patate", oldPatate, patate);
	}
}
