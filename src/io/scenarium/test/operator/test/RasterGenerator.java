/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.operator.test;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import io.beanmanager.editors.DynamicPossibilities;
import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.UpdatableViewBean;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.scenarium.flow.EvolvedOperator;
import io.scenarium.image.struct.raster.ByteRaster;
import io.scenarium.image.struct.raster.DoubleRaster;
import io.scenarium.image.struct.raster.FloatRaster;
import io.scenarium.image.struct.raster.IntegerRaster;
import io.scenarium.image.struct.raster.Raster;
import io.scenarium.image.struct.raster.ShortRaster;
import io.scenarium.test.internal.Log;

public class RasterGenerator extends EvolvedOperator implements UpdatableViewBean {
	@PropertyInfo(index = 0)
	private int width;
	@PropertyInfo(index = 1)
	private int height;
	@PropertyInfo(index = 2, nullable = false)
	@DynamicPossibilities(possibleChoicesMethod = "getImageDataTypes")
	private String imageDataType;
	@PropertyInfo(index = 3, nullable = false)
	@DynamicPossibilities(possibleChoicesMethod = "getImageTypes")
	private String imageType;
	@PropertyInfo(index = 4)
	@NumberInfo(min = 0)
	private long period = 40;
	private Timer timer;
	private double cpt = 0;
	private boolean stop;

	@Override
	public void updateIOStructure() {
		// "double", "float", "int", "short", "byte"
		Class<? extends Raster> outputType = null;
		if (this.imageDataType != null)
			if (this.imageDataType.equals("double"))
				outputType = DoubleRaster.class;
			else if (this.imageDataType.equals("float"))
				outputType = FloatRaster.class;
			else if (this.imageDataType.equals("int"))
				outputType = IntegerRaster.class;
			else if (this.imageDataType.equals("short"))
				outputType = ShortRaster.class;
			else if (this.imageDataType.equals("byte"))
				outputType = ByteRaster.class;
		if (outputType == null)
			updateOutputs(new String[0], new Class[0]);
		else
			updateOutputs(new String[] { "Raster" }, new Class[] { outputType });
	}

	@Override
	public void birth() throws IOException {
		this.stop = false;
		onResume(() -> this.stop = false);
		onPause(() -> this.stop = true);
		onStart(() -> {
			this.timer = new Timer();
			this.timer.scheduleAtFixedRate(new TimerTask() {

				@Override
				public void run() {
					Raster out = null;
					int imageType = getType(RasterGenerator.this.imageType);
					// cpt += 0.05f;
					RasterGenerator.this.cpt = 0;
					if (RasterGenerator.this.imageDataType.equals("double")) {
						DoubleRaster r = new DoubleRaster(RasterGenerator.this.width, RasterGenerator.this.height, imageType);
						double delta = imageType == Raster.YUV ? -0.5 : 0;
						for (int w = 0; w < r.getWidth(); w++)
							for (int h = 0; h < r.getHeight(); h++)
								if (r.getDepth() == 1)
									r.set(w, h, 0, RasterGenerator.this.cpt + w / (double) r.getWidth());
								else {
									r.set(w, h, 0, RasterGenerator.this.cpt);
									r.set(w, h, 1, w / (double) r.getWidth() + delta);
									r.set(w, h, 2, 1 - h / (double) r.getHeight() + delta);
								}
						out = r;
					} else if (RasterGenerator.this.imageDataType.equals("float")) {
						FloatRaster r = new FloatRaster(RasterGenerator.this.width, RasterGenerator.this.height, imageType);
						double delta = imageType == Raster.YUV ? -0.5 : 0;
						for (int w = 0; w < r.getWidth(); w++)
							for (int h = 0; h < r.getHeight(); h++)
								if (r.getDepth() == 1)
									r.set(w, h, 0, (float) (RasterGenerator.this.cpt + w / (float) r.getWidth()));
								else {
									r.set(w, h, 0, (float) RasterGenerator.this.cpt);
									r.set(w, h, 1, (float) (w / (float) r.getWidth() + delta));
									r.set(w, h, 2, (float) (1 - h / (float) r.getHeight() + delta));
								}
						out = r;
					} else if (RasterGenerator.this.imageDataType.equals("int")) {
						IntegerRaster r = new IntegerRaster(RasterGenerator.this.width, RasterGenerator.this.height, imageType);
						for (int w = 0; w < r.getWidth(); w++)
							for (int h = 0; h < r.getHeight(); h++)
								if (r.getDepth() == 1) {
									if (imageType == IntegerRaster.ABGR8 || imageType == IntegerRaster.ARGB8) {
										int a = 255;
										int b = (int) (RasterGenerator.this.cpt * 255);
										int c = (int) ((float) w / r.getWidth() * 255);
										int d = (int) ((1 - (float) h / r.getHeight()) * 255);
										r.set(w, h, 0, a << 24 | b << 16 | c << 8 | d);
									} else
										r.set(w, h, 0, (int) (long) ((RasterGenerator.this.cpt + w / (double) r.getWidth()) * (Integer.MAX_VALUE * 2L + 1L)));
								} else {
									r.set(w, h, 0, (int) (long) (RasterGenerator.this.cpt * (Integer.MAX_VALUE * 2L + 1L)));
									r.set(w, h, 1, (int) (long) (w / (double) r.getWidth() * (Integer.MAX_VALUE * 2L + 1L)));
									r.set(w, h, 2, (int) (long) ((1 - h / (double) r.getHeight()) * (Integer.MAX_VALUE * 2L + 1L)));
								}
						out = r;
					} else if (RasterGenerator.this.imageDataType.equals("short")) {
						ShortRaster r = new ShortRaster(RasterGenerator.this.width, RasterGenerator.this.height, imageType);
						for (int w = 0; w < r.getWidth(); w++)
							for (int h = 0; h < r.getHeight(); h++)
								if (r.getDepth() == 1)
									r.set(w, h, 0, (short) (int) ((RasterGenerator.this.cpt + w / (double) r.getWidth()) * (Short.MAX_VALUE * 2 + 1)));
								else {
									r.set(w, h, 0, (short) (int) (RasterGenerator.this.cpt * (Short.MAX_VALUE * 2L + 1)));
									r.set(w, h, 1, (short) (int) (w / (double) r.getWidth() * (Short.MAX_VALUE * 2L + 1)));
									r.set(w, h, 2, (short) (int) ((1 - h / (double) r.getHeight()) * (Short.MAX_VALUE * 2 + 1)));
								}
						out = r;
					} else if (RasterGenerator.this.imageDataType.equals("byte")) {
						ByteRaster r = new ByteRaster(RasterGenerator.this.width, RasterGenerator.this.height, imageType);
						for (int w = 0; w < r.getWidth(); w++)
							for (int h = 0; h < r.getHeight(); h++)
								if (r.getDepth() == 1)
									r.set(w, h, 0, (byte) (int) ((RasterGenerator.this.cpt + w / (double) r.getWidth()) * (Byte.MAX_VALUE * 2 + 1)));
								else {
									r.set(w, h, 0, (byte) (int) (RasterGenerator.this.cpt * (Byte.MAX_VALUE * 2L + 1)));
									r.set(w, h, 1, (byte) (int) (w / (double) r.getWidth() * (Byte.MAX_VALUE * 2L + 1)));
									r.set(w, h, 2, (byte) (int) ((1 - h / (double) r.getHeight()) * (Byte.MAX_VALUE * 2 + 1)));
								}
						out = r;
					}

					if (RasterGenerator.this.cpt > 1)
						RasterGenerator.this.cpt = 0;
					if (!RasterGenerator.this.stop && out != null)
						triggerOutput(out);
				}

				private int getType(String imageType) {
					switch (imageType) {
					case "GRAY":
						return 0;
					case "RGB":
						return 1;
					case "BGR":
						return 2;
					case "YUV":
						return 3;
					case "ARGB8":
						return 4;
					case "ABGR8":
						return 5;
					default:
						throw new IllegalArgumentException("Unexpected value: " + imageType);
					}
				}
			}, 0, this.period);
		});
	}

	public static void main(String[] args) {
		Log.info("" + (int) Double.NaN);
		long a = Integer.MAX_VALUE * 2L + 1;
		Log.info("" + (int) (a * 0.5));
		Log.info("" + (int) (long) (a * 0.8));
	}

	public void process() {}

	@Override
	public void death() {
		if (this.timer != null) {
			this.timer.cancel();
			this.timer.purge();
			this.timer = null;
		}
	}

	public int getHeight() {
		return this.height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return this.width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public String getImageDataType() {
		return this.imageDataType;
	}

	public void setImageDataType(String imageDataType) {
		this.imageDataType = imageDataType;
		updateView();
		updateIOStructure();
	}

	public String[] getImageDataTypes() {
		return new String[] { "double", "float", "int", "short", "byte" };
	}

	public String getImageType() {
		return this.imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public String[] getImageTypes() {
		if (this.imageDataType != null && this.imageDataType.equals("int"))
			return new String[] { "GRAY", "RGB", "BGR", "YUV", "ARGB8", "ABGR8" };
		return new String[] { "GRAY", "RGB", "BGR", "YUV" };
	}

	public long getPeriod() {
		return this.period;
	}

	public void setPeriod(long period) {
		this.period = period;
	}

	@Override
	public void updateView() {
		fireUpdateView(this, "imageType", false);
	}
}
