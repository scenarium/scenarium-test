package io.scenarium.test.operator.test;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

import io.scenarium.test.internal.Log;

public class GenericBug {
	private GenericBug() {}

	public static void main(String[] args) {
		ObjectManager<Foo> objectManager1 = createFooManager(Foo.class);
		Log.info(objectManager1.getDescription());

		ObjectManager<Object> objectManager2 = createFooManager(Object.class);
		Log.info(objectManager2.getDescription());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static <T> ObjectManager<T> createFooManager(Class<T> type) {
		if (Foo.class.isAssignableFrom(type))
			return new FooManager(type);
		return new ObjectManager<>(type);
	}
}

class ObjectManager<T> {
	public T val;

	public ObjectManager(Class<T> type) {
		try {
			this.val = type.getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
	}

	public String getDescription() {
		return "Hash: " + Objects.hash(this.val);
	}
}

class FooManager<T extends Foo> extends ObjectManager<T> {

	public FooManager(Class<T> type) {
		super(type);
	}

	@Override
	public String getDescription() {
		return super.getDescription() + " value: " + this.val.getVal();
	}
}

class Foo {
	private final double val = Math.random();

	public Foo() {}

	public double getVal() {
		return this.val;
	}
}
