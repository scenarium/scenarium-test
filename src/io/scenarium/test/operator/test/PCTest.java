package io.scenarium.test.operator.test;

import io.scenarium.flow.EvolvedOperator;
import io.scenarium.core.struct.OldObject3D;

public class PCTest extends EvolvedOperator {
	@Override
	public void birth() {
		onStart(() -> triggerOutput(new OldObject3D(OldObject3D.POINTS, new float[] { 5, 0, 1 }, 3)));
	}

	public OldObject3D process() {
		return null;
	}

	@Override
	public void death() {}
}
