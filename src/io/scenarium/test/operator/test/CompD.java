/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.operator.test;

public class CompD {
	static Integer res = Integer.valueOf(0);

	public void birth() {

	}

	public void death() {

	}

	public int getOutputA() {
		return 0;
	}

	public int getOutputB() {
		return 1;
	}

	public int getOutputC() {
		return 2;
	}

	public int getOutputD() {
		return 3;
	}

	public int getOutputE() {
		return 4;
	}

	public int getOutputF() {
		return 5;
	}

	public int getOutputG() {
		return 6;
	}

	public int getOutputH() {
		return 7;
	}

	public int getOutputI() {
		return 8;
	}

	public int getOutputJ() {
		return 9;
	}

	public Integer process(Integer a, Integer b, Integer c, Integer d, Integer e, Integer f, Integer g, Integer h, Integer i, Integer j) {
		// int sum = 0;
		// if (a != null)
		// sum += a;
		// if (b != null)
		// sum += b;
		// if (c != null)
		// sum += c;
		// if (d != null)
		// sum += d;
		// if (e != null)
		// sum += e;
		// if (f != null)
		// sum += f;
		// if (g != null)
		// sum += g;
		// if (h != null)
		// sum += h;
		// if (i != null)
		// sum += i;
		// if (j != null)
		// sum += j;
		return res;
	}
}
