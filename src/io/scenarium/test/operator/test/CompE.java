/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.operator.test;

public class CompE {
	static Integer res = Integer.valueOf(0);

	public void birth() {

	}

	public void death() {

	}

	public int getOutputA() {
		return 0;
	}

	public int getOutputB() {
		return 1;
	}

	public int getOutputC() {
		return 2;
	}

	public int getOutputD() {
		return 3;
	}

	public Integer process(Integer r) {
		return res;
	}
}
