/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.operator.test;

import java.awt.image.BufferedImage;

import io.scenarium.core.tools.TimeStampSynchronizer;
import io.scenarium.flow.EvolvedOperator;
import io.scenarium.test.internal.Log;

public class VictorOp extends EvolvedOperator {
	private TimeStampSynchronizer timeStampSync;

	@Override
	public void birth() {
		this.timeStampSync = new TimeStampSynchronizer(2, this::getTimeStamp);
	}

	public Double process(BufferedImage img, int[] tirets, Double proj) {
		this.timeStampSync.feed((objects, timeStamp) -> {
			if (proj != null)
				Log.info("" + proj);
		}, img, tirets);
		return null;
	}

	@Override
	public void death() {}
}
