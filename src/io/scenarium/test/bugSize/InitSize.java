/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.bugSize;

import io.scenarium.test.internal.Log;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class InitSize extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Scene s = new Scene(new Button("cool"), 320, 240);
		primaryStage.setScene(s);
		primaryStage.heightProperty().addListener(e -> {
			Log.info("" + primaryStage.getHeight());
		});
		primaryStage.setOnShown(e -> Log.info("show"));
		primaryStage.show();
	}
}