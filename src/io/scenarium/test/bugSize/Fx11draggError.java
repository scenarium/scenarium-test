/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.bugSize;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.TransferMode;
import javafx.stage.Stage;

//Workaround: -Djdk.gtk.version=2
public class Fx11draggError extends Application {

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage stage) throws Exception {
		Button button = new Button("test");
		button.setOnDragDetected(e -> {
			ClipboardContent content = new ClipboardContent();
			content.putString("It works");
			button.startDragAndDrop(TransferMode.MOVE).setContent(content);
			e.consume();
		});
		stage.setScene(new Scene(button, 200, 200));
		stage.show();
	}
}
