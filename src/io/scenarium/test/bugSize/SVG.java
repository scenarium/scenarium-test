/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.bugSize;

import java.io.File;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class SVG extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		WebView webView = new WebView();
		webView.setZoom(1);
		webView.setPrefSize(256, 256);
		webView.getEngine().load(new File("/home/revilloud/workspace/ScenariumFx/resource/scenarium.svg").toURI().toString());
		webView.zoomProperty().bind(webView.widthProperty().divide(256));
		Scene scene = new Scene(webView);
		primaryStage.setScene(scene);
		primaryStage.show();
		// ScenicView.show(scene);
	}

}
