/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.bugSize;

import io.scenarium.test.internal.Log;

import org.scenicview.ScenicView;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.skin.VirtualFlow;
import javafx.stage.Stage;

public class TreeViewSample extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		TreeView<String> tv = new TreeView<>(new TreeItem<>("ugheuorjltjdsffelbeisfmjsekfnmkzerlknkzmefzkerksfsfvbg")) {
			protected void layoutChildren() {
				super.layoutChildren();
				Log.info("layout");
				if (getChildrenUnmodifiable().size() == 1) {
					VirtualFlow<?> t = (VirtualFlow<?>) getChildrenUnmodifiable().get(0);
					t.autosize();
					setPrefWidth(t.prefWidth(Double.MAX_VALUE));
					setPrefHeight(t.prefHeight(Double.MAX_VALUE));
				}

			}
		};
		tv.getChildrenUnmodifiable().addListener((InvalidationListener) observable -> {
			Log.info("" + tv.getChildrenUnmodifiable().size());
			if (tv.getChildrenUnmodifiable().size() == 1) {
				VirtualFlow<?> t = (VirtualFlow<?>) tv.getChildrenUnmodifiable().get(0);
				// tv.prefHeightProperty().bind(t.prefHeightProperty());
				// tv.prefWidthProperty().bind(t.prefWidthProperty());

				t.autosize();
				tv.setPrefWidth(t.prefWidth(Double.MAX_VALUE) + 2);
				tv.setPrefHeight(t.prefHeight(Double.MAX_VALUE));

				t.prefWidthProperty().addListener(e -> {
					Log.info("" + t.prefWidthProperty());
				});
				Log.info("" + t.prefWidthProperty().get());
				primaryStage.sizeToScene();
			}
		});
		Log.info("" + tv.getChildrenUnmodifiable().size());
		// Node t = tv.getChildrenUnmodifiable().get(0);
		// tv.prefWidthProperty().bind(tv.getChildrenUnmodifiable().get(0));
		tv.setPadding(new Insets(0));
		primaryStage.setScene(new Scene(tv, -1, -1));
		primaryStage.sizeToScene();
		primaryStage.show();
		ScenicView.show(primaryStage.getScene());
		new Thread(() -> {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			Platform.runLater(() -> {
				VirtualFlow<?> t = (VirtualFlow<?>) tv.getChildrenUnmodifiable().get(0);
				Log.info("" + t.prefWidth(100));
				primaryStage.sizeToScene();
			});
		}).start();
	}

}
