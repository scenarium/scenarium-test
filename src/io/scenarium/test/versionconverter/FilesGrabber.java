/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.versionconverter;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.function.Function;

public class FilesGrabber {

	private FilesGrabber() {}

	public static HashSet<File> getAllFiles(File rootFile, Function<String, Boolean> nameMatcher) {
		LinkedList<File> fileStack = new LinkedList<>();
		HashSet<File> psfStack = new HashSet<>();
		if (rootFile.isDirectory())
			fileStack.push(rootFile);
		else
			psfStack.add(rootFile);
		while (!fileStack.isEmpty()) {
			File file = fileStack.pop();
			if (file.isDirectory() && !Files.isSymbolicLink(file.toPath())) {
				File[] files = file.listFiles();
				if (files != null)
					fileStack.addAll(Arrays.asList(file.listFiles()));
			} else if (nameMatcher.apply(file.getName()))
				psfStack.add(new File(file.getAbsolutePath()));
		}
		return psfStack;
	}
}
