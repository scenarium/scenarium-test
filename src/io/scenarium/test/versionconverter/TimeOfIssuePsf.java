/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.versionconverter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;

import io.scenarium.core.filemanager.scenariomanager.LocalScenario;
import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.filemanager.scenariomanager.ScenarioManager;
import io.scenarium.pluginManager.ModuleManager;
import io.scenarium.test.internal.Log;

public class TimeOfIssuePsf {

	private TimeOfIssuePsf() {}

	public static void main(String[] args) throws IOException {
		ModuleManager.birth();
		updateTree(new File("/home/revilloud/Mansart/Datasets/VEDECOM/Scenic_Kurt/Scenarium"));
	}

	public static void updateTree(File rootFile) throws IOException {
		HashSet<File> files = FilesGrabber.getAllFiles(rootFile, fileName -> fileName.endsWith(".psf") && fileName.startsWith("Rec"));
		files.removeIf(f -> f.getAbsolutePath().contains("20190114"));
		for (File file : files) {
			Log.info("update file: " + file);
			File f = new File(file.getParent() + File.separator + "New" + file.getName());
			try (RandomAccessFile raf = new RandomAccessFile(f, "rw")) {
				raf.writeUTF("patate");
				try (FileInputStream fis = new FileInputStream(file)) {
					FileChannel inChannel = fis.getChannel();
					MappedByteBuffer buffer = inChannel.map(FileChannel.MapMode.READ_ONLY, 0, inChannel.size());
					buffer.load();
					String scenarioPath = file.getParent();
					HashSet<String> scenarioNames = new HashSet<>();
					readCompactHeader(buffer, scenarioPath, scenarioNames);
				}
			}
			Log.info("" + files);
		}
	}

	private static void readCompactHeader(MappedByteBuffer buffer, String scenarioPath, HashSet<String> scenarioNames) {
		buffer.get();
		int nbInput = buffer.getInt();
		for (int i = 0; i < nbInput; i++) {
			String filePath = getString(buffer);
			String scenarioName = getScenarioName(filePath);
			if (scenarioNames.contains(scenarioName)) {
				Log.error("This scenario already include the outputs: " + scenarioName);
				continue;
			}
			scenarioNames.add(scenarioName);
			filePath = scenarioPath + File.separator + filePath;
			if (!new File(filePath).exists())
				continue;
			Class<? extends Scenario> scenarioType = ScenarioManager.getScenarioType(new File(filePath));
			if (scenarioType == null) {
				Log.error("No loader for the file: " + filePath);
				continue;
			} else if (!LocalScenario.class.isAssignableFrom(scenarioType)) {
				Log.error("Not a LocalScenario type");
				continue;
			}
		}
	}

	private static String getString(ByteBuffer bb) {
		byte[] stringBytes = new byte[bb.getInt()];
		bb.get(stringBytes);
		return new String(stringBytes, StandardCharsets.UTF_8);
	}

	public static String getScenarioName(String filePath) {
		return filePath.substring(0, filePath.lastIndexOf("."));
	}
}
