/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test;

import java.nio.ByteBuffer;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelBuffer;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class NativeRendering extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		int width = 640;
		int height = 480;
		byte[] buf = new byte[width * height * 4];
		for (int i = 0; i < buf.length; i += 4) {
			buf[i] = (byte) 255;
			buf[i + 1] = 0;
			buf[i + 2] = 0;
			buf[i + 3] = (byte) 255;
		}
		ByteBuffer byteBuffer = ByteBuffer.wrap(buf);
		PixelFormat<ByteBuffer> pixelFormat = PixelFormat.getByteBgraPreInstance();
		PixelBuffer<ByteBuffer> pixelBuffer = new PixelBuffer<>(width, height, byteBuffer, pixelFormat);
		WritableImage img = new WritableImage(pixelBuffer);
		Scene scene = new Scene(new VBox(new ImageView(img)), -1, -1);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
