/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;

import io.scenarium.test.internal.Log;

public class WatchServiceTest {

	private WatchServiceTest() {}

	public static void main(String[] args) throws IOException, InterruptedException {
		WatchService watchService = FileSystems.getDefault().newWatchService();
		Path testPath = Path.of("/home/marc/test");
		testPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);

		Log.info("Start watchService");
		WatchKey key;
		while ((key = watchService.take()) != null) {
			ArrayList<Path> createdPath = new ArrayList<>();
			List<WatchEvent<?>> newEvents = key.pollEvents();
			Log.info("------new event received: " + newEvents.size() + "-------");
			for (WatchEvent<?> event : newEvents) {
				Kind<?> kind = event.kind();
				Path path = testPath.resolve((Path) event.context());
				if (kind == StandardWatchEventKinds.ENTRY_CREATE) {
					Log.info("ENTRY_CREATE: " + path);
					createdPath.add(path);
				} else if (kind == StandardWatchEventKinds.ENTRY_DELETE)
					Log.info("ENTRY_DELETE: " + path);
				else if (kind == StandardWatchEventKinds.ENTRY_MODIFY) {
					Log.info("ENTRY_MODIFY: " + path);
					if (!createdPath.contains(path))
						Log.info("ENTRY_MODIFY2: " + path);
				}
			}
			key.reset();
			Log.info("-----------------------------------");
			Log.info("");
		}
	}
}
