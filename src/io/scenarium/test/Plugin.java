package io.scenarium.test;

import io.beanmanager.testbean.TypeTest;
import io.scenarium.flow.consumer.OperatorConsumer;
import io.scenarium.flow.consumer.PluginsFlowSupplier;
import io.scenarium.pluginManager.PluginsSupplier;
import io.scenarium.test.operator.test.ArrayGenerator;
import io.scenarium.test.operator.test.B1;
import io.scenarium.test.operator.test.B2;
import io.scenarium.test.operator.test.CompA;
import io.scenarium.test.operator.test.CompB;
import io.scenarium.test.operator.test.CompC;
import io.scenarium.test.operator.test.CompD;
import io.scenarium.test.operator.test.CompE;
import io.scenarium.test.operator.test.DoubleConsumer;
import io.scenarium.test.operator.test.LifeGame;
import io.scenarium.test.operator.test.ListenerTest;
import io.scenarium.test.operator.test.MyOperator;
import io.scenarium.test.operator.test.PCTest;
import io.scenarium.test.operator.test.PersonBean;
import io.scenarium.test.operator.test.RasterGenerator;
import io.scenarium.test.operator.test.RmiOperatorTest;
import io.scenarium.test.operator.test.io.EvolvedIOTest;
import io.scenarium.test.operator.test.io.SimpleIOTest;

public class Plugin implements PluginsSupplier, PluginsFlowSupplier {
	@Override
	public void populateOperators(OperatorConsumer operatorConsumer) {
		// operatorConsumer.accept(NMEA0183Decoder.class);
		operatorConsumer.accept(TypeTest.class);
		operatorConsumer.accept(LifeGame.class);
		operatorConsumer.accept(MyOperator.class);
		operatorConsumer.accept(PCTest.class);
		operatorConsumer.accept(PersonBean.class);
		operatorConsumer.accept(RmiOperatorTest.class);
		operatorConsumer.accept(ArrayGenerator.class);
		operatorConsumer.accept(EvolvedIOTest.class);
		operatorConsumer.accept(SimpleIOTest.class);
		operatorConsumer.accept(CompA.class);
		operatorConsumer.accept(CompB.class);
		operatorConsumer.accept(CompC.class);
		operatorConsumer.accept(CompD.class);
		operatorConsumer.accept(CompE.class);
		operatorConsumer.accept(DoubleConsumer.class);
		operatorConsumer.accept(B1.class);
		operatorConsumer.accept(B2.class);
		operatorConsumer.accept(RasterGenerator.class);
		operatorConsumer.accept(ListenerTest.class);
	}

}
