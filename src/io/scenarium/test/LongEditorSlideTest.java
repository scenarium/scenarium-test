/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test;

import io.beanmanager.editors.primitive.number.ControlType;
import io.beanmanager.editors.primitive.number.LongEditor;
import io.scenarium.test.internal.Log;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class LongEditorSlideTest extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		LongEditor longEditor = new LongEditor(Long.MIN_VALUE, Long.MAX_VALUE, ControlType.SLIDER);

		longEditor.addPropertyChangeListener(() -> {
			Log.info("" + longEditor.getValue());

		});
		Scene scene = new Scene(longEditor.getEditor(), -1, -1);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
