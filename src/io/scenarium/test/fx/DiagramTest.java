/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.fx;

import io.scenarium.test.internal.Log;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class DiagramTest extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	private static void populatePane(Pane pane) {
		TextField tf = new TextField("patate");
		tf.focusedProperty().addListener(e -> Log.info("change"));
		pane.getChildren().add(tf);

		DrawableBlock block = new DrawableBlock(50, 50);

		CubicCurve cubic = new CubicCurve(0.0, 0.0, 25.0, 0.0, 75.0, 100.0, 0, 0);
		cubic.setStroke(Color.BLUE);
		cubic.setFill(null);
		cubic.endXProperty().bind(block.translateXProperty().add(block.getInput().centerXProperty()));
		cubic.endYProperty().bind(block.translateYProperty().add(block.getInput().centerYProperty()));
		cubic.setOnMousePressed(e -> Log.info("link"));

		pane.setOnScroll(e -> {
			if (e.getDeltaY() != 0)
				if (e.getDeltaY() < 0) {
					block.setScaleX(0.5);
					block.setScaleY(0.5);
				} else {
					block.setScaleX(2);
					block.setScaleY(2);
				}
		});
		pane.getChildren().addAll(cubic, block, new DrawableBlock(100, 150));
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// setUserAgentStylesheet(STYLESHEET_CASPIAN);
		// AquaFx.style();
		primaryStage.setTitle("Tool Test");
		Pane pane = new Pane();
		populatePane(pane);
		Scene scene = new Scene(pane, 800, 600);
		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		primaryStage.show();
	}
}

class DrawableBlock extends Group {
	private final Circle input;

	public DrawableBlock(double d, double f) {
		Stop[] stops = new Stop[] { new Stop(0, Color.GRAY), new Stop(0.1, Color.GRAY), new Stop(0.1001, Color.GRAY.brighter()), new Stop(1, Color.GRAY.brighter()) };
		LinearGradient lg1 = new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, stops);
		Rectangle blockCore = new Rectangle(100, 200, lg1);
		blockCore.setArcHeight(20);
		blockCore.setArcWidth(20);
		blockCore.setEffect(new DropShadow());

		this.input = new Circle(0, 50, 5, Color.YELLOW);
		this.input.setStroke(Color.GREY);
		Label text = new Label("Lecteur");
		// text.translateXProperty().bind(block.widthProperty().divide(2).subtract(text.widthProperty().divide(2)));
		text.prefWidthProperty().bind(blockCore.widthProperty());
		text.setAlignment(Pos.CENTER);
		text.setCenterShape(true);
		text.setOnMouseClicked(e -> {
			if (e.getClickCount() == 2)
				Log.info("text");
		});

		this.input.setOnMousePressed(e -> Log.info("input"));
		blockCore.setOnMousePressed(e -> Log.info("block"));
		blockCore.setOnMouseDragged(e -> {
			Log.info("" + e.getX());
			setTranslateX(e.getSceneX() - blockCore.getWidth() / 2);
			setTranslateY(e.getSceneY() - blockCore.getHeight() / 2);
		});
		setTranslateX(d);
		setTranslateY(f);

		setCache(true);
		getChildren().addAll(blockCore, this.input, text);
	}

	public Circle getInput() {
		return this.input;
	}
}
