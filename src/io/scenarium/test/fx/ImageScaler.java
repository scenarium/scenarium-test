/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.fx;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class ImageScaler extends Application {
	private static final String IMAGE_LOC = "http://icons.iconarchive.com/icons/martin-berube/character/32/Robot-icon.png";

	private static final int SCALE_FACTOR = 6;

	public static void main(String[] args) {
		Application.launch(args);
	}

	private Image image;

	private int scaledImageSize;

	private static Label centeredLabel(String text) {
		Label label = new Label(text);
		GridPane.setHalignment(label, HPos.CENTER);

		return label;
	}

	@Override
	public void init() {
		this.image = new Image(IMAGE_LOC);

		this.scaledImageSize = (int) this.image.getWidth() * SCALE_FACTOR;
	}

	private static Image resample(Image input, int scaleFactor) {
		final int w = (int) input.getWidth();
		final int h = (int) input.getHeight();
		final int s = scaleFactor;

		WritableImage output = new WritableImage(w * s, h * s);

		PixelReader reader = input.getPixelReader();
		PixelWriter writer = output.getPixelWriter();

		for (int y = 0; y < h; y++)
			for (int x = 0; x < w; x++) {
				final int argb = reader.getArgb(x, y);
				for (int dy = 0; dy < s; dy++)
					for (int dx = 0; dx < s; dx++)
						writer.setArgb(x * s + dx, y * s + dy, argb);
			}

		return output;
	}

	@Override
	public void start(Stage stage) {
		GridPane layout = new GridPane();
		layout.setHgap(10);
		layout.setVgap(10);

		ImageView originalImageView = new ImageView(this.image);
		StackPane originalImageViewStack = new StackPane();
		originalImageViewStack.getChildren().add(originalImageView);
		originalImageViewStack.setMinWidth(this.scaledImageSize);

		ImageView fittedImageView = new ImageView(this.image);
		fittedImageView.setSmooth(false);
		fittedImageView.setFitWidth(this.scaledImageSize);
		fittedImageView.setFitHeight(this.scaledImageSize);

		ImageView scaledImageView = new ImageView(this.image);
		scaledImageView.setSmooth(false);
		scaledImageView.setScaleX(SCALE_FACTOR);
		scaledImageView.setScaleY(SCALE_FACTOR);
		Group scaledImageViewGroup = new Group(scaledImageView);

		ImageView sizedImageInView = new ImageView(new Image(IMAGE_LOC, 200, 200, false, false));

		ImageView resampledImageView = new ImageView(resample(this.image, SCALE_FACTOR));

		layout.addRow(0, withTooltip(originalImageViewStack, "Unmodified image"), withTooltip(sizedImageInView, "Image sized in Image constructor - Image smoothing false"),
				withTooltip(fittedImageView, "Image fitted using ImageView fitWidth/fitHeight - ImageView smoothing false"),
				withTooltip(scaledImageViewGroup, "ImageView scaled with Node scaleX/scaleY - ImageView smoothing false"),
				withTooltip(resampledImageView, "Image manually recreated as a new WritableImage using a PixelWriter"));

		layout.addRow(1, centeredLabel("Original"), centeredLabel("Sized"), centeredLabel("Fitted"), centeredLabel("Scaled"), centeredLabel("Resampled"));
		layout.setAlignment(Pos.CENTER);

		layout.setStyle("-fx-background-color: cornsilk; -fx-padding: 10;");
		stage.setScene(new Scene(layout));
		stage.show();
	}

	private static Node withTooltip(Node node, String text) {
		Tooltip.install(node, new Tooltip(text));
		return node;
	}
}