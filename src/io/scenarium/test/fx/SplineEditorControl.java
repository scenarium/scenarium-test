/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.fx;

import io.beanmanager.ihmtest.FxTest;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.scene.Cursor;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

public final class SplineEditorControl extends XYChart<Number, Number> {

	private static double clamp(double value) {
		return value < 0 ? 0 : value > 1 ? 1 : value;
	}

	public static void main(String[] args) {
		FxTest.launchIHM(args, s -> new SplineEditorControl());
	}

	private final DoubleProperty controlPoint1x = new SimpleDoubleProperty(0.8);

	private final DoubleProperty controlPoint1y = new SimpleDoubleProperty(0.2);

	private final DoubleProperty controlPoint2x = new SimpleDoubleProperty(0.2);

	private final DoubleProperty controlPoint2y = new SimpleDoubleProperty(0.8);

	private final Circle controlPoint1Circle;

	private final Circle controlPoint2Circle;

	private final Line cp1Line;

	private final Line cp2Line;

	private final Path dottedLinesPath;

	private final Path splinePath;

	public SplineEditorControl() {
		super(new NumberAxis(0, 1, 0.1), new NumberAxis(0, 1, 0.1));
		this.controlPoint1Circle = createCircle(Color.WHITE, Color.RED, 3d, 6d, Cursor.HAND);
		this.controlPoint2Circle = createCircle(Color.WHITE, Color.RED, 3d, 6d, Cursor.HAND);

		this.cp1Line = createLine(Color.RED, 2);
		this.cp2Line = createLine(Color.RED, 2);

		this.dottedLinesPath = createPath(Color.GREY, 1d, 1d, 8d);
		this.splinePath = createPath(Color.BLACK, 2d, 1d, 1d);
		initialize();
	}

	public DoubleProperty controlPoint1xProperty() {
		return this.controlPoint1x;
	}

	public DoubleProperty controlPoint1yProperty() {
		return this.controlPoint1y;
	}

	public DoubleProperty controlPoint2xProperty() {
		return this.controlPoint2x;
	}

	public DoubleProperty controlPoint2yProperty() {
		return this.controlPoint2y;
	}

	private static Circle createCircle(Color fill, Color stroke, double width, double radius, Cursor cursor) {
		Circle circle = new Circle();
		circle.fillProperty().set(fill);
		circle.setStroke(stroke);
		circle.strokeWidthProperty().set(width);
		circle.setRadius(radius);
		circle.setCursor(cursor);

		return circle;
	}

	private static Line createLine(Color stroke, double width) {
		Line line = new Line();
		line.setStroke(stroke);
		line.strokeWidthProperty().set(width);
		return line;
	}

	private static Path createPath(Color stroke, double width, double a, double b) {
		Path path = new Path();
		path.setStroke(stroke);
		path.strokeWidthProperty().set(width);
		path.getStrokeDashArray().addAll(a, b);
		return path;
	}

	@Override
	protected void dataItemAdded(Series<Number, Number> series, int i, Data<Number, Number> data) {}

	@Override
	protected void dataItemChanged(Data<Number, Number> data) {}

	@Override
	protected void dataItemRemoved(Data<Number, Number> data, Series<Number, Number> series) {}

	public double getControlPoint1x() {
		return this.controlPoint1x.get();
	}

	public double getControlPoint1y() {
		return this.controlPoint1y.get();
	}

	public double getControlPoint2x() {
		return this.controlPoint2x.get();
	}

	public double getControlPoint2y() {
		return this.controlPoint2y.get();
	}

	public void initialize() {
		setAnimated(false);
		getXAxis().setAutoRanging(false);
		getXAxis().setAutoRanging(false);
		setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		setAlternativeRowFillVisible(false);
		getPlotChildren().addAll(this.splinePath, this.dottedLinesPath, this.cp1Line, this.cp2Line, this.controlPoint1Circle, this.controlPoint2Circle);
		setData(FXCollections.observableArrayList());

		this.controlPoint1Circle.setOnMouseDragged((MouseEvent event) -> {
			final double x = event.getX() + this.controlPoint1Circle.getLayoutX();
			final double y = event.getY() + this.controlPoint1Circle.getLayoutY();
			final double dataX = getXAxis().getValueForDisplay(x).doubleValue();
			final double dataY = getYAxis().getValueForDisplay(y).doubleValue();
			this.controlPoint1x.set(clamp(dataX));
			this.controlPoint1y.set(clamp(dataY));
			requestChartLayout();
		});
		this.controlPoint2Circle.setOnMouseDragged((MouseEvent event) -> {
			final double x = event.getX() + this.controlPoint2Circle.getLayoutX();
			final double y = event.getY() + this.controlPoint2Circle.getLayoutY();
			final double dataX = getXAxis().getValueForDisplay(x).doubleValue();
			final double dataY = getYAxis().getValueForDisplay(y).doubleValue();
			this.controlPoint2x.set(clamp(dataX));
			this.controlPoint2y.set(clamp(dataY));
			requestChartLayout();
		});
	}

	@Override
	protected void layoutPlotChildren() {
		double cp1x = getXAxis().getDisplayPosition(this.controlPoint1x.get());
		double cp1y = getYAxis().getDisplayPosition(this.controlPoint1y.get());
		double cp2x = getXAxis().getDisplayPosition(this.controlPoint2x.get());
		double cp2y = getYAxis().getDisplayPosition(this.controlPoint2y.get());
		double minx = getXAxis().getZeroPosition();
		double miny = getYAxis().getZeroPosition();
		double maxx = getXAxis().getDisplayPosition(1);
		double maxy = getYAxis().getDisplayPosition(1);
		this.controlPoint1Circle.setLayoutX(cp1x);
		this.controlPoint1Circle.setLayoutY(cp1y);
		this.controlPoint2Circle.setLayoutX(cp2x);
		this.controlPoint2Circle.setLayoutY(cp2y);
		this.cp1Line.setStartX(minx);
		this.cp1Line.setStartY(miny);
		this.cp1Line.setEndX(cp1x);
		this.cp1Line.setEndY(cp1y);
		this.cp2Line.setStartX(maxx);
		this.cp2Line.setStartY(maxy);
		this.cp2Line.setEndX(cp2x);
		this.cp2Line.setEndY(cp2y);
		this.dottedLinesPath.getElements().setAll(new MoveTo(minx - 0.5, cp1y - 0.5), new LineTo(cp1x - 0.5, cp1y - 0.5), new LineTo(cp1x - 0.5, miny - 0.5), new MoveTo(minx - 0.5, cp2y - 0.5),
				new LineTo(cp2x - 0.5, cp2y - 0.5), new LineTo(cp2x - 0.5, miny - 0.5));
		this.splinePath.getElements().setAll(new MoveTo(minx, miny), new CubicCurveTo(cp1x, cp1y, cp2x, cp2y, maxx, maxy));
	}

	@Override
	protected void seriesAdded(Series<Number, Number> series, int i) {}

	@Override
	protected void seriesRemoved(Series<Number, Number> series) {}
}
