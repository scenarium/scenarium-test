/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.fx;

import org.scenicview.ScenicView;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.Window;

public class FxTestSizeToScene extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		final StackPane theaterPane = new StackPane();
		theaterPane.setPrefSize(2000, 2000);
		Scene scene = new Scene(theaterPane, -1, -1);
		primaryStage.setScene(scene);
		primaryStage.show();
		ScenicView.show(scene);
		new Thread(() -> {
			while (true)
				try {
					Thread.sleep(5000);
					Platform.runLater(() -> {
						Window windows = scene.getWindow();
						theaterPane.setPrefSize(1, 1);
						if (windows instanceof Stage) {
							Stage stage = (Stage) windows;
							stage.sizeToScene();
						}
						theaterPane.setPrefSize(2000, 2000);
						if (windows instanceof Stage) {
							Stage stage = (Stage) windows;
							stage.sizeToScene();
						}
					});
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}).start();
	}
}
