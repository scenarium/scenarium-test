/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.test.fx;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Test extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Tool Test");
		GridPane gridPane = new GridPane();
		gridPane.add(new Label("Cool"), 0, 0);
		gridPane.add(new Label("Cool1"), 0, 1);

		gridPane.add(new Label("Cool2"), 1, 0);
		gridPane.add(new Label("Cool3"), 1, 1);

		primaryStage.setScene(new Scene(gridPane, -1, -1));
		primaryStage.sizeToScene();
		gridPane.prefWidthProperty().addListener(e -> Platform.runLater(() -> primaryStage.sizeToScene()));
		gridPane.prefHeightProperty().addListener(e -> Platform.runLater(() -> primaryStage.sizeToScene()));
		primaryStage.show();
	}
}
